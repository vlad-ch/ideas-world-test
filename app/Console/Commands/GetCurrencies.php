<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GetCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Getting actual currency rates from http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://www.cbr.ru/scripts/XML_daily.asp');

        $source = iconv('Windows-1251', 'UTF-8//IGNORE', $response->body());
        $source = mb_convert_encoding($source, 'HTML-ENTITIES', 'UTF-8');

        $xml = simplexml_load_string($source);

        $date = (array)$xml->attributes()->Date;
        $date = date('Y-m-d', strtotime($date[0]));

        foreach ($xml as $element) {
            Currency::updateOrCreate([
                'name' => $element->CharCode,
                'rate' => (float)str_replace(',', '.', $element->Value),
                'created_at' => $date
            ]);
        }

        return 0;
    }
}
