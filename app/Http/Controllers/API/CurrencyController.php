<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CurrencyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $products = Currency::paginate(10);
        return $this->sendResponse($products->toArray(), 'Данные успешно получены.');
    }

    /**
     * Display the specified resource.
     *
     * @param string $date
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $date): JsonResponse
    {
        $currency = Currency::query()->where('created_at', $date)->get();
        if (is_null($currency)) {
            return $this->sendError('Ошибка при получении данных.');
        }
        return $this->sendResponse($currency->toArray(), 'Данные успешно получены.');
    }
}
