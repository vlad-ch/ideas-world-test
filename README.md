## Запуск:
make init

## Получение курсов валют:
make get-currencies  
или  
docker-compose exec app php artisan currencies:get

## Проверка задач в планировщике:
docker-compose exec app php artisan schedule:list

## Примеры API запросов:
http://localhost/api/register?name=user&email=user@mail.ru&password=password&c_password=password  
http://localhost/api/currencies?page=2  
http://localhost/api/currency/2021-08-12
