up: memory
	docker-compose up -d

down:
	docker-compose down

build: memory
	docker-compose up --build -d

test:
	docker-compose exec app vendor/bin/phpunit

init: up perm composer-install assets-install assets-dev cp-env migrate
	docker-compose exec app php artisan key:generate
	docker-compose exec app php artisan passport:install

composer-install:
	docker-compose exec app composer i

composer-update:
	docker-compose exec app composer u

cp-env:
	cp .env.example .env

migrate:
	docker-compose exec app php artisan migrate

get-currencies:
	docker-compose exec app php artisan currencies:get

assets-install:
	docker-compose exec node yarn install

assets-rebuild:
	docker-compose exec node npm rebuild node-sass --force

assets-dev:
	docker-compose exec node yarn run dev

assets-watch:
	docker-compose exec node yarn run watch

memory:
	sudo sysctl -w vm.max_map_count=262144

perm:
	sudo chown -R $(USER):$(USER) .
	sudo chgrp -R www-data storage bootstrap/cache
	sudo chmod -R ug+rwx storage bootstrap/cache
