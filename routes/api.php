<?php

use App\Http\Controllers\API\CurrencyController;
use App\Http\Controllers\API\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register'])->action;
Route::middleware('auth:api')->group(function () {
    Route::get('currencies', [CurrencyController::class, 'index']);
    Route::get('currency/{date}', [CurrencyController::class, 'show']);
});
